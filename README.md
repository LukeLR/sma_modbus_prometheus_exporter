# SMA Modbus Prometheus exporter

Export SMA solar metrics from Modbus for Prometheus

## Introduction

This repo contains a complete configuration file [modbus.yml](modbus.yml) to export SMA Modbus metrics for prometheus using [modbus_exporter](https://github.com/RichiH/modbus_exporter). The config file is automatically generated from the official SMA modbus documentation available [here](https://www.sma.de/produkte/hybrid-wechselrichter/sunny-tripower-smart-energy) ([direct download link](https://files.sma.de/downloads/PARAMETER-HTML_STPxx-3SE-40_30109R_V11.zip)), so it contains all exposed modbus metrics as documented by SMA.

## Visualisation using Grafana

An example Grafana dashboard is included in [grafana_example_dashboard.yml](grafana_example_dashboard.yml) and looks somewhat like this:
![Screenshot 1 of the Grafana example dashboard](grafana_example_dashboard-01.png)
![Screenshot 2 of the Grafana example dashboard](grafana_example_dashboard-02.png)
![Screenshot 3 of the Grafana example dashboard](grafana_example_dashboard-03.png)
![Screenshot 4 of the Grafana example dashboard](grafana_example_dashboard-04.png)
![Screenshot 5 of the Grafana example dashboard](grafana_example_dashboard-05.png)
![Screenshot 6 of the Grafana example dashboard](grafana_example_dashboard-06.png)
![Screenshot 7 of the Grafana example dashboard](grafana_example_dashboard-07.png)

## Basic usage

1. Clone or download this repository recursively, to include the modbus_exporter submodule, and `cd` into it:
    ```
    git clone --recursive https://codeberg.org/LukeLR/sma_modbus_prometheus_exporter
    ```
2. `cd` into the `modbus_exporter` folder and build according to their [README](https://github.com/RichiH/modbus_exporter/blob/main/README.md):
    ```
    cd sma_modbus_prometheus_exporter/modbus_exporter
    make build
    ```
3. Execude `modbus_exporter` with the configuration file supplied by this repository_
    ```
    ./modbus_exporter --config.file="../modbus.yml"
    ```
4. Point your browser or `curl` to `localhost:9602/modbus` and supply the IP address of the SMA device, the module name (`sma_sunny_tripower` as specified in [modbus.yml](modbus.yml)) and the sub-device ID (default is 3 for SMA) as parameters:
    ```
    curl http://localhost:9602/modbus?target=<SMA-IP>:502&module=sma_sunny_tripower&sub_target=3
    ```
5. You should get all SMA modbus metrics in prometheus format similar to this:
    <details>
    <summary>Complete `modbus_exporter` output</summary>
    <pre><code>
    # HELP modbus_exporter_sma_bat_amp Battery current
    # TYPE modbus_exporter_sma_bat_amp gauge
    modbus_exporter_sma_bat_amp{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_capacrtgwh Battery nominal capacity
    # TYPE modbus_exporter_sma_bat_capacrtgwh gauge
    modbus_exporter_sma_bat_capacrtgwh{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_chaampsptdmd Maximum charging current
    # TYPE modbus_exporter_sma_bat_chaampsptdmd gauge
    modbus_exporter_sma_bat_chaampsptdmd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_chactlcomaval Control of battery charging via communication available
    # TYPE modbus_exporter_sma_bat_chactlcomaval gauge
    modbus_exporter_sma_bat_chactlcomaval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_chastt Current battery state of charge
    # TYPE modbus_exporter_sma_bat_chastt gauge
    modbus_exporter_sma_bat_chastt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_chavolsptdmd End-of-charge voltage
    # TYPE modbus_exporter_sma_bat_chavolsptdmd gauge
    modbus_exporter_sma_bat_chavolsptdmd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_diag_actlcapacnom Current battery capacity
    # TYPE modbus_exporter_sma_bat_diag_actlcapacnom gauge
    modbus_exporter_sma_bat_diag_actlcapacnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_dschampsptdmd Maximum discharging current
    # TYPE modbus_exporter_sma_bat_dschampsptdmd gauge
    modbus_exporter_sma_bat_dschampsptdmd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_dschvolsptdmd End-of-discharge voltage
    # TYPE modbus_exporter_sma_bat_dschvolsptdmd gauge
    modbus_exporter_sma_bat_dschvolsptdmd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_opstt Battery oper. status
    # TYPE modbus_exporter_sma_bat_opstt gauge
    modbus_exporter_sma_bat_opstt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_tmpval Battery temperature
    # TYPE modbus_exporter_sma_bat_tmpval gauge
    modbus_exporter_sma_bat_tmpval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_typ Battery type
    # TYPE modbus_exporter_sma_bat_typ gauge
    modbus_exporter_sma_bat_typ{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_bat_vol Battery voltage
    # TYPE modbus_exporter_sma_bat_vol gauge
    modbus_exporter_sma_bat_vol{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batchrg_actbatchrg Charge of current battery
    # TYPE modbus_exporter_sma_batchrg_actbatchrg gauge
    modbus_exporter_sma_batchrg_actbatchrg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batchrg_batchrg Battery charge
    # TYPE modbus_exporter_sma_batchrg_batchrg gauge
    modbus_exporter_sma_batchrg_batchrg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batchrg_curbatcha Present battery charge
    # TYPE modbus_exporter_sma_batchrg_curbatcha gauge
    modbus_exporter_sma_batchrg_curbatcha{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batconv_wmaxcha Batt. manuf's max. charge capac.
    # TYPE modbus_exporter_sma_batconv_wmaxcha gauge
    modbus_exporter_sma_batconv_wmaxcha{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batconv_wmaxdsch Batt. manuf's max. disch. capac.
    # TYPE modbus_exporter_sma_batconv_wmaxdsch gauge
    modbus_exporter_sma_batconv_wmaxdsch{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batdsch_actbatdsch Battery discharge of current battery
    # TYPE modbus_exporter_sma_batdsch_actbatdsch gauge
    modbus_exporter_sma_batdsch_actbatdsch{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batdsch_batdsch Battery discharge
    # TYPE modbus_exporter_sma_batdsch_batdsch gauge
    modbus_exporter_sma_batdsch_batdsch{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batdsch_curbatdsch Present battery discharge
    # TYPE modbus_exporter_sma_batdsch_curbatdsch gauge
    modbus_exporter_sma_batdsch_curbatdsch{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batusdm_bckdmmin Minimum width of backup power area
    # TYPE modbus_exporter_sma_batusdm_bckdmmin gauge
    modbus_exporter_sma_batusdm_bckdmmin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batusdm_dschprodmmin Minimum width of deep discharge protection area
    # TYPE modbus_exporter_sma_batusdm_dschprodmmin gauge
    modbus_exporter_sma_batusdm_dschprodmmin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_batusdm_stt Status battery application area
    # TYPE modbus_exporter_sma_batusdm_stt gauge
    modbus_exporter_sma_batusdm_stt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cmpbms_batchamaxw Max. battery charge capac.
    # TYPE modbus_exporter_sma_cmpbms_batchamaxw gauge
    modbus_exporter_sma_cmpbms_batchamaxw{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cmpbms_batchawh Charge energy
    # TYPE modbus_exporter_sma_cmpbms_batchawh gauge
    modbus_exporter_sma_cmpbms_batchawh{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cmpbms_batdschmaxw Max. battery discharge capac.
    # TYPE modbus_exporter_sma_cmpbms_batdschmaxw gauge
    modbus_exporter_sma_cmpbms_batdschmaxw{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cmpbms_batdschwh Discharge energy
    # TYPE modbus_exporter_sma_cmpbms_batdschwh gauge
    modbus_exporter_sma_cmpbms_batdschwh{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cmpbms_getbatchawh Current charging power
    # TYPE modbus_exporter_sma_cmpbms_getbatchawh gauge
    modbus_exporter_sma_cmpbms_getbatchawh{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cmpbms_getbatdschwh Current discharging power
    # TYPE modbus_exporter_sma_cmpbms_getbatdschwh gauge
    modbus_exporter_sma_cmpbms_getbatdschwh{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_cntrysettings_lang Language of the user interface
    # TYPE modbus_exporter_sma_cntrysettings_lang gauge
    modbus_exporter_sma_cntrysettings_lang{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_coolsys_cab_tmpval Internal temperature
    # TYPE modbus_exporter_sma_coolsys_cab_tmpval gauge
    modbus_exporter_sma_coolsys_cab_tmpval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_dcms_amp DC current input
    # TYPE modbus_exporter_sma_dcms_amp gauge
    modbus_exporter_sma_dcms_amp{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_dcms_vol DC voltage input
    # TYPE modbus_exporter_sma_dcms_vol gauge
    modbus_exporter_sma_dcms_vol{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_dcms_watt DC power input
    # TYPE modbus_exporter_sma_dcms_watt gauge
    modbus_exporter_sma_dcms_watt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_dttm_dlsvison Standard/Daylight saving time conversion on
    # TYPE modbus_exporter_sma_dttm_dlsvison gauge
    modbus_exporter_sma_dttm_dlsvison{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_dttm_tm Systemzeit
    # TYPE modbus_exporter_sma_dttm_tm gauge
    modbus_exporter_sma_dttm_tm{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_dttm_tmzn Time zone
    # TYPE modbus_exporter_sma_dttm_tmzn gauge
    modbus_exporter_sma_dttm_tmzn{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_env_exinsol Insolation on external sensor
    # TYPE modbus_exporter_sma_env_exinsol gauge
    modbus_exporter_sma_env_exinsol{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_env_horwspd Wind speed
    # TYPE modbus_exporter_sma_env_horwspd gauge
    modbus_exporter_sma_env_horwspd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_env_tmpval Ambient temperature
    # TYPE modbus_exporter_sma_env_tmpval gauge
    modbus_exporter_sma_env_tmpval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_env_tmpvalmax Highest measured ambient temperature
    # TYPE modbus_exporter_sma_env_tmpvalmax gauge
    modbus_exporter_sma_env_tmpvalmax{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry Country standard set
    # TYPE modbus_exporter_sma_gridguard_cntry gauge
    modbus_exporter_sma_gridguard_cntry{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_hhlim Frequency monitoring median maximum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_hhlim gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_hhlim{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_hhlimtmms Frq. monitoring median max. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_hhlimtmms gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_hhlimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_hlim Frequency monitoring, lower maximum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_hlim gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_hlim{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_hlimtmms Frequency monitoring, lower max. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_hlimtmms gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_hlimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_llim Frequency monitoring upper minimum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_llim gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_llim{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_llimtmms Frequency monitoring, upper min. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_llimtmms gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_llimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_lllim Frequency monitoring median minimum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_lllim gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_lllim{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_lllimtmms Frq. monitoring median min. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_lllimtmms gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_lllimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_max Frequency monitoring upper maximum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_max gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_max{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_maxtmms Frequency monitoring upper max. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_maxtmms gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_maxtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_min Frequency monitoring, lower minimum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_min gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_min{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_mintmms Frequency monitoring, lower min. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_mintmms gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_mintmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_reconmax Maximum switching frequency
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_reconmax gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_reconmax{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_frqctl_reconmin Minimum switching frequency
    # TYPE modbus_exporter_sma_gridguard_cntry_frqctl_reconmin gauge
    modbus_exporter_sma_gridguard_cntry_frqctl_reconmin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_grifltmontms Reconnection time after grid fault
    # TYPE modbus_exporter_sma_gridguard_cntry_grifltmontms gauge
    modbus_exporter_sma_gridguard_cntry_grifltmontms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_gristrtms Reconnection time upon restart
    # TYPE modbus_exporter_sma_gridguard_cntry_gristrtms gauge
    modbus_exporter_sma_gridguard_cntry_gristrtms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_hzrtg Nominal frequency
    # TYPE modbus_exporter_sma_gridguard_cntry_hzrtg gauge
    modbus_exporter_sma_gridguard_cntry_hzrtg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_leakrismin Minimum insulation resistance
    # TYPE modbus_exporter_sma_gridguard_cntry_leakrismin gauge
    modbus_exporter_sma_gridguard_cntry_leakrismin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_hhlimpu Voltage monitoring, median maximum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_hhlimpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_hhlimpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_hhlimtmms Voltage monitoring, median max. threshold trip.time
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_hhlimtmms gauge
    modbus_exporter_sma_gridguard_cntry_volctl_hhlimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_hlimpu Voltage monitoring, lower maximum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_hlimpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_hlimpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_hlimtmms Voltage monitoring, lower max. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_hlimtmms gauge
    modbus_exporter_sma_gridguard_cntry_volctl_hlimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_llimpu Voltage monitoring, upper minimum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_llimpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_llimpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_llimtmms Voltage monitoring, lower min. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_llimtmms gauge
    modbus_exporter_sma_gridguard_cntry_volctl_llimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_lllimpu Voltage monitoring, median minimum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_lllimpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_lllimpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_lllimtmms Voltage monitoring, median min. threshold trip.time
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_lllimtmms gauge
    modbus_exporter_sma_gridguard_cntry_volctl_lllimtmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_maxpu Voltage monitoring, upper maximum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_maxpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_maxpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_maxputmms Voltage monitoring, upper max. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_maxputmms gauge
    modbus_exporter_sma_gridguard_cntry_volctl_maxputmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_minpu Voltage monitoring, lower minimum threshold
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_minpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_minpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_mintmms Voltage monitoring, lower min. threshold trip. time
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_mintmms gauge
    modbus_exporter_sma_gridguard_cntry_volctl_mintmms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_reconmaxpu Max. voltage for reconnection
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_reconmaxpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_reconmaxpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntry_volctl_reconminpu Min. voltage for reconnection
    # TYPE modbus_exporter_sma_gridguard_cntry_volctl_reconminpu gauge
    modbus_exporter_sma_gridguard_cntry_volctl_reconminpu{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridguard_cntryset Set country standard
    # TYPE modbus_exporter_sma_gridguard_cntryset gauge
    modbus_exporter_sma_gridguard_cntryset{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_a_phsa Grid current phase L1
    # TYPE modbus_exporter_sma_gridms_a_phsa gauge
    modbus_exporter_sma_gridms_a_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_a_phsb Grid current phase L2
    # TYPE modbus_exporter_sma_gridms_a_phsb gauge
    modbus_exporter_sma_gridms_a_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_a_phsc Grid current phase L3
    # TYPE modbus_exporter_sma_gridms_a_phsc gauge
    modbus_exporter_sma_gridms_a_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_hz Grid frequency
    # TYPE modbus_exporter_sma_gridms_hz gauge
    modbus_exporter_sma_gridms_hz{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_phv_phsa Grid voltage phase L1
    # TYPE modbus_exporter_sma_gridms_phv_phsa gauge
    modbus_exporter_sma_gridms_phv_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_phv_phsa2b Grid voltage phase L1 against L2
    # TYPE modbus_exporter_sma_gridms_phv_phsa2b gauge
    modbus_exporter_sma_gridms_phv_phsa2b{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_phv_phsb Grid voltage phase L2
    # TYPE modbus_exporter_sma_gridms_phv_phsb gauge
    modbus_exporter_sma_gridms_phv_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_phv_phsb2c Grid voltage phase L2 against L3
    # TYPE modbus_exporter_sma_gridms_phv_phsb2c gauge
    modbus_exporter_sma_gridms_phv_phsb2c{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_phv_phsc Grid voltage phase L3
    # TYPE modbus_exporter_sma_gridms_phv_phsc gauge
    modbus_exporter_sma_gridms_phv_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_phv_phsc2a Grid voltage phase L3 against L1
    # TYPE modbus_exporter_sma_gridms_phv_phsc2a gauge
    modbus_exporter_sma_gridms_phv_phsc2a{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_tota Grid current
    # TYPE modbus_exporter_sma_gridms_tota gauge
    modbus_exporter_sma_gridms_tota{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_totpfeei EEI displacement power factor
    # TYPE modbus_exporter_sma_gridms_totpfeei gauge
    modbus_exporter_sma_gridms_totpfeei{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_totpfprc Displacement power factor
    # TYPE modbus_exporter_sma_gridms_totpfprc gauge
    modbus_exporter_sma_gridms_totpfprc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_totva Apparent power
    # TYPE modbus_exporter_sma_gridms_totva gauge
    modbus_exporter_sma_gridms_totva{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_totvar Reactive power
    # TYPE modbus_exporter_sma_gridms_totvar gauge
    modbus_exporter_sma_gridms_totvar{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_totw Power
    # TYPE modbus_exporter_sma_gridms_totw gauge
    modbus_exporter_sma_gridms_totw{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_va_phsa Apparent power L1
    # TYPE modbus_exporter_sma_gridms_va_phsa gauge
    modbus_exporter_sma_gridms_va_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_va_phsb Apparent power L2
    # TYPE modbus_exporter_sma_gridms_va_phsb gauge
    modbus_exporter_sma_gridms_va_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_va_phsc Apparent power L3
    # TYPE modbus_exporter_sma_gridms_va_phsc gauge
    modbus_exporter_sma_gridms_va_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_var_phsa Reactive power L1
    # TYPE modbus_exporter_sma_gridms_var_phsa gauge
    modbus_exporter_sma_gridms_var_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_var_phsb Reactive power L2
    # TYPE modbus_exporter_sma_gridms_var_phsb gauge
    modbus_exporter_sma_gridms_var_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_var_phsc Reactive power L3
    # TYPE modbus_exporter_sma_gridms_var_phsc gauge
    modbus_exporter_sma_gridms_var_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_w_phsa Power L1
    # TYPE modbus_exporter_sma_gridms_w_phsa gauge
    modbus_exporter_sma_gridms_w_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_w_phsb Power L2
    # TYPE modbus_exporter_sma_gridms_w_phsb gauge
    modbus_exporter_sma_gridms_w_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_gridms_w_phsc Power L3
    # TYPE modbus_exporter_sma_gridms_w_phsc gauge
    modbus_exporter_sma_gridms_w_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_adptcrvreq Apply configuration of curve parameters
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_adptcrvreq gauge
    modbus_exporter_sma_inverter_ctlcomcfg_adptcrvreq{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_adptcrvrsl Result of application of curve parameter configuration
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_adptcrvrsl gauge
    modbus_exporter_sma_inverter_ctlcomcfg_adptcrvrsl{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_ctlmssrc Source of ref. meas. for reactive/active power reduction
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_ctlmssrc gauge
    modbus_exporter_sma_inverter_ctlcomcfg_ctlmssrc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_ctlcommssmod External cos φ setting, fallback behavior
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_ctlcommssmod gauge
    modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_ctlcommssmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_flbpf External cos φ setpoint specification, fallback value of cos φ for active power output
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_flbpf gauge
    modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_flbpf{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_flbpfext External cos φ setpoint specification, fallback value of the excitation type for active power output
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_flbpfext gauge
    modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_flbpfext{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_tmsout External cos φ setting, timeout
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_tmsout gauge
    modbus_exporter_sma_inverter_ctlcomcfg_pfctlcom_tmsout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_ctlcommssmod External reactive power setting, fallback behavior
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_ctlcommssmod gauge
    modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_ctlcommssmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_flbvarnom External reactive power setting, fallback value
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_flbvarnom gauge
    modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_flbvarnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_tmsout External reactive power setting, timeout
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_tmsout gauge
    modbus_exporter_sma_inverter_ctlcomcfg_varctlcom_tmsout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_ctlcommssmod External active power setting, fallback behavior
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_ctlcommssmod gauge
    modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_ctlcommssmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_flbwnom Fallback active power limit P in % of WMax for absent active power limit
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_flbwnom gauge
    modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_flbwnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_tmsout External active power setting, timeout
    # TYPE modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_tmsout gauge
    modbus_exporter_sma_inverter_ctlcomcfg_wctlcom_tmsout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_dgsmodcfg_hystvolnom Hysteresis voltage, dynamic grid support configuration
    # TYPE modbus_exporter_sma_inverter_dgsmodcfg_hystvolnom gauge
    modbus_exporter_sma_inverter_dgsmodcfg_hystvolnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_dgsmodcfg_pwrcirinoptms PWM inversion delay, dynamic grid support configuration
    # TYPE modbus_exporter_sma_inverter_dgsmodcfg_pwrcirinoptms gauge
    modbus_exporter_sma_inverter_dgsmodcfg_pwrcirinoptms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_dgsmodcfg_pwrcirinopvolnom PWM inverse voltage, dynamic grid support configuration
    # TYPE modbus_exporter_sma_inverter_dgsmodcfg_pwrcirinopvolnom gauge
    modbus_exporter_sma_inverter_dgsmodcfg_pwrcirinopvolnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_fststop Fast shut-down
    # TYPE modbus_exporter_sma_inverter_fststop gauge
    modbus_exporter_sma_inverter_fststop{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_pflimq1 Minimum achievable cos(phi) quadrant 1
    # TYPE modbus_exporter_sma_inverter_pflimq1 gauge
    modbus_exporter_sma_inverter_pflimq1{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_pflimq4 Minimum achievable cos(phi) quadrant 4
    # TYPE modbus_exporter_sma_inverter_pflimq4 gauge
    modbus_exporter_sma_inverter_pflimq4{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_plntctl_vref Grid nominal voltage
    # TYPE modbus_exporter_sma_inverter_plntctl_vref gauge
    modbus_exporter_sma_inverter_plntctl_vref{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_plntctl_vrefofs Reference correction voltage, PV system control
    # TYPE modbus_exporter_sma_inverter_plntctl_vrefofs gauge
    modbus_exporter_sma_inverter_plntctl_vrefofs{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_utilcrvcfg_crv0_numpt No. of charac. pt.s to be used
    # TYPE modbus_exporter_sma_inverter_utilcrvcfg_crv0_numpt gauge
    modbus_exporter_sma_inverter_utilcrvcfg_crv0_numpt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_utilcrvcfg_crvpt3_xval X values charact. curve 3
    # TYPE modbus_exporter_sma_inverter_utilcrvcfg_crvpt3_xval gauge
    modbus_exporter_sma_inverter_utilcrvcfg_crvpt3_xval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_utilcrvcfg_crvpt3_yval Y values charact. curve 3
    # TYPE modbus_exporter_sma_inverter_utilcrvcfg_crvpt3_yval gauge
    modbus_exporter_sma_inverter_utilcrvcfg_crvpt3_yval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_valim Maximum apparent power device
    # TYPE modbus_exporter_sma_inverter_valim gauge
    modbus_exporter_sma_inverter_valim{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_vamax Currently set apparent power limit
    # TYPE modbus_exporter_sma_inverter_vamax gauge
    modbus_exporter_sma_inverter_vamax{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varlimq1 Maximum achievable reactive power quadrant 1
    # TYPE modbus_exporter_sma_inverter_varlimq1 gauge
    modbus_exporter_sma_inverter_varlimq1{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varlimq4 Maximum achievable reactive power quadrant 4
    # TYPE modbus_exporter_sma_inverter_varlimq4 gauge
    modbus_exporter_sma_inverter_varlimq4{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pf External cos φ setpoint specification, cos φ setpoint for active power output
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pf gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pf{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pfeei Setpoint cos(phi) as per EEI convention
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pfeei gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pfeei{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pfext External cos φ setpoint specification, excitation type for active power output
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pfext gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlcomcfg_pfext{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_dyn_vargraneg cos φ(P), decrease rate
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_dyn_vargraneg gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_dyn_vargraneg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_dyn_vargrapos cos φ(P), increase rate
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_dyn_vargrapos gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_dyn_vargrapos{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfextstop Excit. type at end point, cos φ(P) char. config.
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfextstop gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfextstop{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfextstr Excit. type at start point, cos φ(P) char. conf.
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfextstr gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfextstr{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfstop cos φ at end point, cos φ(P) char. config.
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfstop gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfstop{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfstr cos φ at start point, cos φ(P) char. config.
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfstr gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_pfstr{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_wnomstop Act. power at end point, cos φ(P) char. config.
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_wnomstop gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_wnomstop{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_wnomstr Act. power at start point, cos φ(P) char. config.
    # TYPE modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_wnomstr gauge
    modbus_exporter_sma_inverter_varmodcfg_pfctlwcfg_wnomstr{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varcnstcfg_var Manual reactive power setting for active power output
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varcnstcfg_var gauge
    modbus_exporter_sma_inverter_varmodcfg_varcnstcfg_var{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varcnstcfg_varnom Manual reactive power setting for active power output
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varcnstcfg_varnom gauge
    modbus_exporter_sma_inverter_varmodcfg_varcnstcfg_varnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlcomcfg_varnom Standardized reactive power setpoint by system control
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlcomcfg_varnom gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlcomcfg_varnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlcomcfg_varnomprc Extern. reactive power mode
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlcomcfg_varnomprc gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlcomcfg_varnomprc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_numpt Q(V), number of support points to be used
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_numpt gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_numpt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_xval Q(V), voltage value
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_xval gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_xval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_yval Q(V), reactive power value
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_yval gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_crv_yval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_dyn_vargraneg Q(V), decrease rate
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_dyn_vargraneg gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_dyn_vargraneg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_dyn_vargrapos Q(V), increase rate
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_dyn_vargrapos gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_dyn_vargrapos{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_vargranom Reactive power gradient, reactive power/voltage characteristic curve configuration Q(V)
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_vargranom gauge
    modbus_exporter_sma_inverter_varmodcfg_varctlvolcfg_vargranom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_varmodcfg_varmod Operating mode of static voltage stability, configuration of static voltage stability
    # TYPE modbus_exporter_sma_inverter_varmodcfg_varmod gauge
    modbus_exporter_sma_inverter_varmodcfg_varmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hystena Activation of stay-set indicator function, linear instantaneous power gradient configuration
    # TYPE modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hystena gauge
    modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hystena{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstop Difference between reset frequency and grid frequency, linear instantaneous power gradient configuration
    # TYPE modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstop gauge
    modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstop{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstopwgra P(f), active power change rate after fault end
    # TYPE modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstopwgra gauge
    modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstopwgra{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstr Difference between starting frequency and grid frequency, linear instantaneous power gradient configuration
    # TYPE modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstr gauge
    modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_hzstr{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_wgra Active power gradient, linear instantaneous power gradient configuration
    # TYPE modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_wgra gauge
    modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzcfg_wgra{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzmod Operating mode of active power reduction in case of overfrequency P(f)
    # TYPE modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzmod gauge
    modbus_exporter_sma_inverter_wctlhzmodcfg_wctlhzmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wgraconn Soft start-up rate P
    # TYPE modbus_exporter_sma_inverter_wgraconn gauge
    modbus_exporter_sma_inverter_wgraconn{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wgrampp Increase rate in case of insolation change
    # TYPE modbus_exporter_sma_inverter_wgrampp gauge
    modbus_exporter_sma_inverter_wgrampp{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wgrarecon Reconnect gradient after grid fault
    # TYPE modbus_exporter_sma_inverter_wgrarecon gauge
    modbus_exporter_sma_inverter_wgrarecon{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wgrareconena Soft start-up P after grid fault
    # TYPE modbus_exporter_sma_inverter_wgrareconena gauge
    modbus_exporter_sma_inverter_wgrareconena{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wlim Rated active power WMaxOutRtg
    # TYPE modbus_exporter_sma_inverter_wlim gauge
    modbus_exporter_sma_inverter_wlim{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmax Set active power limit
    # TYPE modbus_exporter_sma_inverter_wmax gauge
    modbus_exporter_sma_inverter_wmax{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wcnstcfg_w Active power limitation P, active power configuration
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wcnstcfg_w gauge
    modbus_exporter_sma_inverter_wmodcfg_wcnstcfg_w{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wcnstcfg_wnom Active power limitation P, active power configuration
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wcnstcfg_wnom gauge
    modbus_exporter_sma_inverter_wmodcfg_wcnstcfg_wnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgraena External active power setting, limitation of change rate
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgraena gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgraena{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgraneg External active power setting, decrease rate
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgraneg gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgraneg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgrapos External active power setting, increase rate
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgrapos gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_dyn_wgrapos{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_flbwspt External active power setpoint specification, fallback value for active power setpoint specification
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_flbwspt gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_flbwspt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_w Active power limitation by PV system control
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_w gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_w{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wctlcomact Eff./reac. pow. contr. via comm.
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wctlcomact gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wctlcomact{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wnom Normalized active power limitation by PV system control
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wnom gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wnomprc Normalized active power limitation by PV system control
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wnomprc gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wnomprc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wspt Active power setpoint
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wspt gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlcomcfg_wspt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_numpt P(V), number of points used
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_numpt gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_numpt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_xval P(V), voltage value
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_xval gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_xval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_yval P(V), active power value
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_yval gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_crv_yval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_ena P(V), activation
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_ena gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_ena{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wgraneg P(V), decrease rate
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wgraneg gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wgraneg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wgrapos P(V), increase rate
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wgrapos gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wgrapos{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wrefmod P(V), type of reference active power
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wrefmod gauge
    modbus_exporter_sma_inverter_wmodcfg_wctlvolcfg_wrefmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_inverter_wmodcfg_wmod Operating mode active power setting
    # TYPE modbus_exporter_sma_inverter_wmodcfg_wmod gauge
    modbus_exporter_sma_inverter_wmodcfg_wmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_isolation_flta Residual current
    # TYPE modbus_exporter_sma_isolation_flta gauge
    modbus_exporter_sma_isolation_flta{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_isolation_leakris Insulation resistance
    # TYPE modbus_exporter_sma_isolation_leakris gauge
    modbus_exporter_sma_isolation_leakris{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_mb_sunspec_varmoddeptref Dependent reference reactive power setpoint
    # TYPE modbus_exporter_sma_mb_sunspec_varmoddeptref gauge
    modbus_exporter_sma_mb_sunspec_varmoddeptref{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_mb_sunspec_voltvardeptref Dependent reference Q(V) reactive power curve point
    # TYPE modbus_exporter_sma_mb_sunspec_voltvardeptref gauge
    modbus_exporter_sma_mb_sunspec_voltvardeptref{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_mb_sunspec_wattvardeptref Dependent reference Q(P) reactive power curve point
    # TYPE modbus_exporter_sma_mb_sunspec_wattvardeptref gauge
    modbus_exporter_sma_mb_sunspec_wattvardeptref{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_mdul_tmpval Module temperature
    # TYPE modbus_exporter_sma_mdul_tmpval gauge
    modbus_exporter_sma_mdul_tmpval{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_dywhout Daily yield
    # TYPE modbus_exporter_sma_metering_dywhout gauge
    modbus_exporter_sma_metering_dywhout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_a_phsa Grid current phase L1
    # TYPE modbus_exporter_sma_metering_gridms_a_phsa gauge
    modbus_exporter_sma_metering_gridms_a_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_a_phsb Grid current phase L2
    # TYPE modbus_exporter_sma_metering_gridms_a_phsb gauge
    modbus_exporter_sma_metering_gridms_a_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_a_phsc Grid current phase L3
    # TYPE modbus_exporter_sma_metering_gridms_a_phsc gauge
    modbus_exporter_sma_metering_gridms_a_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_hz Grid frequency
    # TYPE modbus_exporter_sma_metering_gridms_hz gauge
    modbus_exporter_sma_metering_gridms_hz{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_phv_phsa Grid voltage phase L1
    # TYPE modbus_exporter_sma_metering_gridms_phv_phsa gauge
    modbus_exporter_sma_metering_gridms_phv_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_phv_phsa2b Grid voltage phase L1 against L2
    # TYPE modbus_exporter_sma_metering_gridms_phv_phsa2b gauge
    modbus_exporter_sma_metering_gridms_phv_phsa2b{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_phv_phsb Grid voltage phase L2
    # TYPE modbus_exporter_sma_metering_gridms_phv_phsb gauge
    modbus_exporter_sma_metering_gridms_phv_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_phv_phsb2c Grid voltage phase L2 against L3
    # TYPE modbus_exporter_sma_metering_gridms_phv_phsb2c gauge
    modbus_exporter_sma_metering_gridms_phv_phsb2c{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_phv_phsc Grid voltage phase L3
    # TYPE modbus_exporter_sma_metering_gridms_phv_phsc gauge
    modbus_exporter_sma_metering_gridms_phv_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_phv_phsc2a Grid voltage phase L3 against L1
    # TYPE modbus_exporter_sma_metering_gridms_phv_phsc2a gauge
    modbus_exporter_sma_metering_gridms_phv_phsc2a{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totpf Displacement power factor
    # TYPE modbus_exporter_sma_metering_gridms_totpf gauge
    modbus_exporter_sma_metering_gridms_totpf{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totpfeei EEI displacement power factor
    # TYPE modbus_exporter_sma_metering_gridms_totpfeei gauge
    modbus_exporter_sma_metering_gridms_totpfeei{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totva Apparent power
    # TYPE modbus_exporter_sma_metering_gridms_totva gauge
    modbus_exporter_sma_metering_gridms_totva{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totvar Reactive power grid feeding
    # TYPE modbus_exporter_sma_metering_gridms_totvar gauge
    modbus_exporter_sma_metering_gridms_totvar{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totwhin Counter reading of power drawn counter
    # TYPE modbus_exporter_sma_metering_gridms_totwhin gauge
    modbus_exporter_sma_metering_gridms_totwhin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totwhout Grid feed-in counter reading
    # TYPE modbus_exporter_sma_metering_gridms_totwhout gauge
    modbus_exporter_sma_metering_gridms_totwhout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totwin Power drawn
    # TYPE modbus_exporter_sma_metering_gridms_totwin gauge
    modbus_exporter_sma_metering_gridms_totwin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_totwout Power grid feed-in
    # TYPE modbus_exporter_sma_metering_gridms_totwout gauge
    modbus_exporter_sma_metering_gridms_totwout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_va_phsa Apparent power L1
    # TYPE modbus_exporter_sma_metering_gridms_va_phsa gauge
    modbus_exporter_sma_metering_gridms_va_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_va_phsb Apparent power L2
    # TYPE modbus_exporter_sma_metering_gridms_va_phsb gauge
    modbus_exporter_sma_metering_gridms_va_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_va_phsc Apparent power L3
    # TYPE modbus_exporter_sma_metering_gridms_va_phsc gauge
    modbus_exporter_sma_metering_gridms_va_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_var_phsa Reactive power grid feeding phase L1
    # TYPE modbus_exporter_sma_metering_gridms_var_phsa gauge
    modbus_exporter_sma_metering_gridms_var_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_var_phsb Reactive power grid feeding phase L2
    # TYPE modbus_exporter_sma_metering_gridms_var_phsb gauge
    modbus_exporter_sma_metering_gridms_var_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_var_phsc Reactive power grid feeding phase L3
    # TYPE modbus_exporter_sma_metering_gridms_var_phsc gauge
    modbus_exporter_sma_metering_gridms_var_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_w_phsa Power grid feeding L1
    # TYPE modbus_exporter_sma_metering_gridms_w_phsa gauge
    modbus_exporter_sma_metering_gridms_w_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_w_phsb Power grid feeding L2
    # TYPE modbus_exporter_sma_metering_gridms_w_phsb gauge
    modbus_exporter_sma_metering_gridms_w_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_w_phsc Power grid feeding L3
    # TYPE modbus_exporter_sma_metering_gridms_w_phsc gauge
    modbus_exporter_sma_metering_gridms_w_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_win_phsa Power drawn from grid phase L1
    # TYPE modbus_exporter_sma_metering_gridms_win_phsa gauge
    modbus_exporter_sma_metering_gridms_win_phsa{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_win_phsb Power drawn from grid phase L2
    # TYPE modbus_exporter_sma_metering_gridms_win_phsb gauge
    modbus_exporter_sma_metering_gridms_win_phsb{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_gridms_win_phsc Power drawn from grid phase L3
    # TYPE modbus_exporter_sma_metering_gridms_win_phsc gauge
    modbus_exporter_sma_metering_gridms_win_phsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_metering_totwhout Total yield
    # TYPE modbus_exporter_sma_metering_totwhout gauge
    modbus_exporter_sma_metering_totwhout{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_mltfncsw_opmode Operating mode of multifunction relay
    # TYPE modbus_exporter_sma_mltfncsw_opmode gauge
    modbus_exporter_sma_mltfncsw_opmode{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_mltfncsw_stt Multifunction relay status
    # TYPE modbus_exporter_sma_mltfncsw_stt gauge
    modbus_exporter_sma_mltfncsw_stt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_artg Nominal current of all phases
    # TYPE modbus_exporter_sma_nameplate_artg gauge
    modbus_exporter_sma_nameplate_artg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_bat_vendor Battery manufacturer
    # TYPE modbus_exporter_sma_nameplate_bat_vendor gauge
    modbus_exporter_sma_nameplate_bat_vendor{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_cmpbms_swrev BMS firmware version
    # TYPE modbus_exporter_sma_nameplate_cmpbms_swrev gauge
    modbus_exporter_sma_nameplate_cmpbms_swrev{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_cmpbms_typ BMS type
    # TYPE modbus_exporter_sma_nameplate_cmpbms_typ gauge
    modbus_exporter_sma_nameplate_cmpbms_typ{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_cmpmain_swrev Firmware version of the main processor
    # TYPE modbus_exporter_sma_nameplate_cmpmain_swrev gauge
    modbus_exporter_sma_nameplate_cmpmain_swrev{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_comrev Communication version
    # TYPE modbus_exporter_sma_nameplate_comrev gauge
    modbus_exporter_sma_nameplate_comrev{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_mainmodel Device class
    # TYPE modbus_exporter_sma_nameplate_mainmodel gauge
    modbus_exporter_sma_nameplate_mainmodel{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_model Device type
    # TYPE modbus_exporter_sma_nameplate_model gauge
    modbus_exporter_sma_nameplate_model{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_pkgrev Software package
    # TYPE modbus_exporter_sma_nameplate_pkgrev gauge
    modbus_exporter_sma_nameplate_pkgrev{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_sernum Seriennummer
    # TYPE modbus_exporter_sma_nameplate_sernum gauge
    modbus_exporter_sma_nameplate_sernum{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_susyid SUSyID Modul
    # TYPE modbus_exporter_sma_nameplate_susyid gauge
    modbus_exporter_sma_nameplate_susyid{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_nameplate_vendor Hersteller
    # TYPE modbus_exporter_sma_nameplate_vendor gauge
    modbus_exporter_sma_nameplate_vendor{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_bat_health Battery status
    # TYPE modbus_exporter_sma_operation_bat_health gauge
    modbus_exporter_sma_operation_bat_health{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_cmpbms_opstt Operating status
    # TYPE modbus_exporter_sma_operation_cmpbms_opstt gauge
    modbus_exporter_sma_operation_cmpbms_opstt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_comrstms Remaining duration until CP restart
    # TYPE modbus_exporter_sma_operation_comrstms gauge
    modbus_exporter_sma_operation_comrstms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evt_dsc Fault correction measure
    # TYPE modbus_exporter_sma_operation_evt_dsc gauge
    modbus_exporter_sma_operation_evt_dsc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evt_evtno Current event number for manufacturer
    # TYPE modbus_exporter_sma_operation_evt_evtno gauge
    modbus_exporter_sma_operation_evt_evtno{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evt_msg Message
    # TYPE modbus_exporter_sma_operation_evt_msg gauge
    modbus_exporter_sma_operation_evt_msg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evt_prio Recommended action
    # TYPE modbus_exporter_sma_operation_evt_prio gauge
    modbus_exporter_sma_operation_evt_prio{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evtcntistl Number of events for installer
    # TYPE modbus_exporter_sma_operation_evtcntistl gauge
    modbus_exporter_sma_operation_evtcntistl{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evtcntsvc Number of events for service
    # TYPE modbus_exporter_sma_operation_evtcntsvc gauge
    modbus_exporter_sma_operation_evtcntsvc{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_evtcntusr Number of events for user
    # TYPE modbus_exporter_sma_operation_evtcntusr gauge
    modbus_exporter_sma_operation_evtcntusr{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_griswstt Grid relay/contactor
    # TYPE modbus_exporter_sma_operation_griswstt gauge
    modbus_exporter_sma_operation_griswstt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_health Condition
    # TYPE modbus_exporter_sma_operation_health gauge
    modbus_exporter_sma_operation_health{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_healthstt_alm Nominal power in Fault Mode
    # TYPE modbus_exporter_sma_operation_healthstt_alm gauge
    modbus_exporter_sma_operation_healthstt_alm{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_healthstt_ok Nominal power in Ok Mode
    # TYPE modbus_exporter_sma_operation_healthstt_ok gauge
    modbus_exporter_sma_operation_healthstt_ok{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_healthstt_wrn Nominal power in Warning Mode
    # TYPE modbus_exporter_sma_operation_healthstt_wrn gauge
    modbus_exporter_sma_operation_healthstt_wrn{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_inverter_health Inverter status
    # TYPE modbus_exporter_sma_operation_inverter_health gauge
    modbus_exporter_sma_operation_inverter_health{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_opmod General operating mode
    # TYPE modbus_exporter_sma_operation_opmod gauge
    modbus_exporter_sma_operation_opmod{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_opstt General operating status
    # TYPE modbus_exporter_sma_operation_opstt gauge
    modbus_exporter_sma_operation_opstt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_pvgriconn Plant mains connection
    # TYPE modbus_exporter_sma_operation_pvgriconn gauge
    modbus_exporter_sma_operation_pvgriconn{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_rmgtms Waiting time until feed-in
    # TYPE modbus_exporter_sma_operation_rmgtms gauge
    modbus_exporter_sma_operation_rmgtms{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_rstrlokstt Block status
    # TYPE modbus_exporter_sma_operation_rstrlokstt gauge
    modbus_exporter_sma_operation_rstrlokstt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_runstt Operating status
    # TYPE modbus_exporter_sma_operation_runstt gauge
    modbus_exporter_sma_operation_runstt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_operation_valrsistl Reset operating data
    # TYPE modbus_exporter_sma_operation_valrsistl gauge
    modbus_exporter_sma_operation_valrsistl{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_pcc_wmax Set active power limit at grid connection point
    # TYPE modbus_exporter_sma_pcc_wmax gauge
    modbus_exporter_sma_pcc_wmax{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_pcc_wmaxnom Set active power limit at grid connection point
    # TYPE modbus_exporter_sma_pcc_wmaxnom gauge
    modbus_exporter_sma_pcc_wmaxnom{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_plnt_dcwrtg Nominal PV system power
    # TYPE modbus_exporter_sma_plnt_dcwrtg gauge
    modbus_exporter_sma_plnt_dcwrtg{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_selfcsmp_batchasttmin Lower discharge limit for self-consumption increase
    # TYPE modbus_exporter_sma_selfcsmp_batchasttmin gauge
    modbus_exporter_sma_selfcsmp_batchasttmin{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_sma_grid_guard_code SMA Grid Guard-Code
    # TYPE modbus_exporter_sma_sma_grid_guard_code gauge
    modbus_exporter_sma_sma_grid_guard_code{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_autocfgison Automatic speedwire configuration switched on
    # TYPE modbus_exporter_sma_spdwr_autocfgison gauge
    modbus_exporter_sma_spdwr_autocfgison{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_comsoca_connspd Connection speed of SMACOM A
    # TYPE modbus_exporter_sma_spdwr_comsoca_connspd gauge
    modbus_exporter_sma_spdwr_comsoca_connspd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_comsoca_dpxmode Duplex mode of SMACOM A
    # TYPE modbus_exporter_sma_spdwr_comsoca_dpxmode gauge
    modbus_exporter_sma_spdwr_comsoca_dpxmode{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_comsoca_stt Speedwire connection status of SMACOM A
    # TYPE modbus_exporter_sma_spdwr_comsoca_stt gauge
    modbus_exporter_sma_spdwr_comsoca_stt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_comsocb_connspd Connection speed of SMACOM B
    # TYPE modbus_exporter_sma_spdwr_comsocb_connspd gauge
    modbus_exporter_sma_spdwr_comsocb_connspd{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_comsocb_dpxmode Duplex mode of SMACOM B
    # TYPE modbus_exporter_sma_spdwr_comsocb_dpxmode gauge
    modbus_exporter_sma_spdwr_comsocb_dpxmode{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_spdwr_comsocb_stt Speedwire connection status of SMACOM B
    # TYPE modbus_exporter_sma_spdwr_comsocb_stt gauge
    modbus_exporter_sma_spdwr_comsocb_stt{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_sys_devrstr Initiate device restart
    # TYPE modbus_exporter_sma_sys_devrstr gauge
    modbus_exporter_sma_sys_devrstr{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_unit_id_des_wechselrichters Unit ID des Wechselrichters
    # TYPE modbus_exporter_sma_unit_id_des_wechselrichters gauge
    modbus_exporter_sma_unit_id_des_wechselrichters{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_upd_autoupdison Automatic updates activated
    # TYPE modbus_exporter_sma_upd_autoupdison gauge
    modbus_exporter_sma_upd_autoupdison{module="sma_sunny_tripower"} <value here>
    # HELP modbus_exporter_sma_versionsnummer_sma_modbus_profil Versionsnummer SMA Modbus-Profil
    # TYPE modbus_exporter_sma_versionsnummer_sma_modbus_profil gauge
    modbus_exporter_sma_versionsnummer_sma_modbus_profil{module="sma_sunny_tripower"} <value here>
    </code></pre>
    </details>
6. Done!

## Installation as a systemd service
Follow the instructions in the [README](https://github.com/LukeLR/modbus_exporter/blob/main/README.md#installation-as-a-systemd-service) of the modbus_exporter repo to install this as a systemd service.
